<?php 
 
 /* This file implements a linked list in PHP.
    For this purpose we have two classes in this file
      1)- link class - the individual node in the list
      2)- LinkedList - implements all linked list functions.
 
  */
 
 class LinkedList{
     //link to first link of linked list
     private $head;
 
     // link to last link of linked list
     private $tail;
 
     // keeping track of number of list items to aid in sorting, inserting, and deleting
     private $item_count;
 
     function __construct(){
 
         $this->head  = NULL;
         $this->tail = NULL;
         $this->item_count  = 0;
     }
 
     public function insert_last($item) {
         $link = new link($item);
 
         if($this->head != NULL) {
 
             $this->tail->next = $link;
             $this->tail= &$link;
             $this->item_count++;
 
         }else {
             $this->insert_first($item);
         }
     }

     public function insert_first($item) {
 
         $link = new link($item);
         if($this->head == NULL){
 
             $this->head= &$link;
             $this->tail = &$link;
 
         } else {
 
             $link->next= $this->head;
             $this->head= &$link;
 
         }
 
         $this->item_count++;
 
     }

     public function insert($item , $position){
 
         if($position <= $this->item_count){
             if($position == 0){
                 $this->insert_first($item);
             }else if($position == $this->item_count){
                 $this->insert_last($item);
             }else{
                 $this->insert_at($item , $position);
             }
 
         }else {
 
             throw new Exception("Index out of bound");
 
         }
 
     }

  
     public function print_list(){
     
         $current = $this->head;         
         while($current !== NULL){
           echo $current->getData();
		   echo("<br>");
           $current  = $current->next;          
         }         
     }
	
     public function delete($index){
 
         if($this->item_count > $index){
             if($index == 0){ 
                 $this->removeFirst();
             }else { 
                 $this->remove($index);
             }
         }else{ 
             throw new Exception("Index out of bounds");
         } 
     }

     public function find($item){
         $status = FALSE;
         $current = $this->head;
         for($i = 0; $i < $this->item_count-1; $i++){
             if($item === $current->getData()){
                 $status = TRUE;
                 break;
             }
             $current = $current->next;
         }
         return $status;
     }

     public function get($index){

         if(($this->size() > $index) && ($index >= 0)){
             $current = $this->head;
             for($i = 0;$i <= $index; $i++){
                if($i == $index){
                    $itemData = $current->getData();
                    return $itemData;
                }
                $current = $current->next;
             }
 
         } else {
 
             throw new Exception("Index out of bounds");
         }
 
     }

     public function size(){
 
        return $this->item_count;
 
     }
 
     public function isEmpty(){
 
        if($this->head == NULL){
            return TRUE;
        }else{
            return FALSE;
        }
     }
 
     function removeFirst(){
 
         $this->head = $this->head->next;
         $this->item_count--;
     }
 
     private function remove($index){
 
         $current = $this->head;
         for($i = 0; $i < $index; $i++ ){
             if($i == $index-1){
                 $toBeRemovedReference = $current->next;
                 $current->next    = $toBeRemovedReference->next;
 
                 if($toBeRemovedReference->next == NULL){  // If it is last link ,update instance
                                                           //   variable
                      $this->tail    = $current;
 
                 }
                 $this->item_count--;
              }
              $current = $current->next;
         }
 
     }
 
     private function insert_at($item , $position){
 
         $current = $this->head;
         $link = new link($item);
         for($i = 1; $i <= $position ; $i++){
 
            if($position == $i){
                $nextlinkReference = $current->next;
                $current->next = $link;
                $link->next = $nextlinkReference;
                $this->item_count++;
                break;
            }
            $current = $current->next;
 
         }
     }

 }
 
class link{
 
     
     public $data;
     // stores address to next link
     public $next;
 
     // link constructor
     function  __construct($item = FALSE){ 
         $this->data = $item;
         $this->next = NULL;
     }
 
     // returns data
     public function getData(){		
		 return $this->data;        
     }
	 
     //standard Java-like toString function
     public function __toString() {
           return "Data: {$this->data}\n";
     }
 
}
  
    /* Here we are implementing our linked list, and printing out the values */

    echo "Linked List<br> ";
        
    $theList = new LinkedList();        
    $theList->insert_last("10,11");
    $theList->insert_last("11,12");
    $theList->insert_last("11,12");
    $theList->insert_last("11,12");

    $theList->print_list();
		
